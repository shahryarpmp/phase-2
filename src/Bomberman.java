import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Bomberman extends Stuff implements Serializable {

    private int speed = 300;

    private int BombLimit = 1;

    private long last_move = 0;

    private boolean ghostMode = false ;

    public Bomberman() {

        try {
            this.setImage(ImageIO.read(new File(thispath + "/src/pictures/Bomberman.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getBombLimit() {
        return BombLimit;
    }

    public void setBombLimit(int bombLimit) {
        BombLimit = bombLimit;
    }

    @Override
    public String toString() {
        return "( " + getxInBoard() + " , " + getyInBoard() + " )";
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setLast_move(long last_move) {
        this.last_move = last_move;
    }

    public boolean isGhostMode() {
        return ghostMode;
    }

    public void setGhostMode(boolean ghostMode) {
        this.ghostMode = ghostMode;
    }

    public boolean permision2move(long thistime, int elapsTime) {

        if (last_move == 0)
            return true;
        else if (thistime - last_move - speed > elapsTime)
            return true;
        return false;

    }

}
