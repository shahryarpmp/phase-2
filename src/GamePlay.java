import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class GamePlay extends JFrame {

    int tool = 0;
    int arz = 0;

    GameInfo gameInfo;
    BoardPanel boardPanel;

    public GamePlay() {
        GameplayInit();
    }

    public GamePlay(int t, int a) {
        tool = t;
        arz = a;
        GameplayInit();
    }

    public void GameplayInit() {

        gameInfo = new GameInfo();
        if (tool == 0)
            boardPanel = new BoardPanel(gameInfo);
        else
            boardPanel = new BoardPanel(gameInfo, tool, arz);

        this.setBounds(0, 0, 1725, 935);
        this.setFocusTraversalKeysEnabled(false);
        this.setTitle("Boomberman");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.BLUE);

        JPanel center = new JPanel();
        center.setBackground(Color.blue);

        this.add(gameInfo, BorderLayout.EAST);
        this.add(center, BorderLayout.CENTER);
        this.add(boardPanel, BorderLayout.CENTER);

        Thread thr = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {

                    if ( gameInfo.exitbool || boardPanel.back2main ){
                        GamePlay.this.dispose();
                        Main main = new Main(false) ;
                        main.setVisible(true);
                        return;
                    }

                    if ( boardPanel.exitthisgame )
                        GamePlay.this.dispose();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thr.start();

    }
}

