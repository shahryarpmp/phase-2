import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Main extends JFrame {

    int count = 0;
    JPanel panel;
    Path currentRelativePath = Paths.get("");
    String thispath = currentRelativePath.toAbsolutePath().toString();

    public Main(boolean musicb) {

        this.setTitle("Boomberman");
        this.setBounds(0, 0, 1725, 935);
        setLayout(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        panel = new JPanel() {
            public void paintComponent(Graphics g) {
                Image img = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };

        Thread msc = new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {

                    playmusic();

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        if (count == 0 && musicb)
            msc.start();
        count ++ ;

        panel.setLayout(null);

        JButton playgame = new JButton();
        JButton info = new JButton();
        JButton exit = new JButton();

        playgame.setText("play");
        info.setText("information");
        exit.setText("EXIT");

        panel.add(playgame);
        panel.add(info);
        panel.add(exit);

        playgame.setBounds(650, 300, 100, 100);
        info.setBounds(650 + 130, 300, 100, 100);
        exit.setBounds(650 + 2 * 130, 300, 100, 100);

        JPanel playgamepanel = new JPanel() {
            public void paintComponent(Graphics g) {
                Image img = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };

        playgamepanel.setLayout(null);

        JButton newGame = new JButton();
        JButton loadGame = new JButton();
        JButton backInplay = new JButton();

        newGame.setText("new game");
        loadGame.setText("load game");
        backInplay.setText("back");

        playgamepanel.add(newGame);
        playgamepanel.add(loadGame);
        playgamepanel.add(backInplay);

        newGame.setBounds(650, 300, 100, 100);
        loadGame.setBounds(650 + 130, 300, 100, 100);
        backInplay.setBounds(650 + 2 * 130, 300, 100, 100);

        JPanel newgamepanel = new JPanel() {
            public void paintComponent(Graphics g) {
                Image img = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };

        newgamepanel.setLayout(null);

        String[] list = new String[11];
        int i = 27;
        int m = 0;
        while (i > 5) {
            list[m++] = "" + i;
            i -= 2;
        }

        String[] list1 = new String[5];
        i = 15;
        m = 0;
        while (i > 5) {
            list1[m++] = "" + i;
            i -= 2;
        }

        JComboBox comboBox = new JComboBox(list);
        JComboBox comboBox1 = new JComboBox(list1);

        final String[] k = new String[1];
        k[0] = (String) comboBox.getSelectedItem();
        final String[] k1 = new String[1];
        k1[0] = (String) comboBox.getSelectedItem();

        final boolean[] tmp = {false};

        Thread thr = new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    if (!k[0].equals((String) comboBox.getSelectedItem()))
                        k[0] = (String) comboBox.getSelectedItem();

                    if (!k1[0].equals((String) comboBox1.getSelectedItem()))
                        k1[0] = (String) comboBox1.getSelectedItem();

                    if (tmp[0])
                        return;

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        comboBox.setBounds(760, 400, 60, 30);
        comboBox1.setBounds(860, 400, 60, 30);
        newgamepanel.add(comboBox);
        newgamepanel.add(comboBox1);

        JTextField height = new JTextField();
        height.setText("height");
        height.setBounds(760, 370, 60, 20);
        height.setEditable(false);

        JTextField width = new JTextField();
        width.setText("width");
        width.setBounds(860, 370, 60, 20);
        width.setEditable(false);

        JTextField bull = new JTextField();
        bull.setText("chose the height and width of board");
        bull.setBounds(710, 270, 250, 40);
        bull.setEditable(false);

        newgamepanel.add(height);
        newgamepanel.add(width);
        newgamepanel.add(bull);

        JButton next = new JButton();
        next.setText("start");
        newgamepanel.add(next);
        next.setBounds(790, 480, 100, 30);

        JButton backInNewgame = new JButton();
        backInNewgame.setText("back");
        backInNewgame.setBounds(790, 540, 100, 30);
        newgamepanel.add(backInNewgame);

        JPanel infopanel = new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);
                Image img1 = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                g.drawImage(img1, 0, 0, this.getWidth(), this.getHeight(), this);

            }

        };

        infopanel.setLayout(null);

        JButton keyboard = new JButton();
        JButton intru = new JButton();
        JButton backfrominfo = new JButton();

        keyboard.setText("keyboard");
        intru.setText("introduction with things");
        backfrominfo.setText("back");

        infopanel.add(intru);
        infopanel.add(keyboard);
        infopanel.add(backfrominfo);

        this.add(infopanel);

        keyboard.setBounds(680, 200, 300, 100);
        intru.setBounds(680, 350, 300, 100);
        backfrominfo.setBounds(680, 500, 300, 100);

        JPanel panelkey = new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);
                BufferedImage img;
                try {
                    Image img1 = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                    g.drawImage(img1, 0, 0, this.getWidth(), this.getHeight(), this);
                    img = ImageIO.read(new File(thispath + "/src/pictures/keyboard.png"));
                    g.drawImage(img, 310, 140, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        };

        panelkey.setLayout(null);
        JButton backfromkey = new JButton();
        backfromkey.setText("back");
        panelkey.add(backfromkey);
        backfromkey.setBounds(800, 800, 100, 30);

        JPanel panelintru = new JPanel() {

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);
                BufferedImage img;
                try {
                    Image img1 = Toolkit.getDefaultToolkit().getImage(thispath + "/src/media5gif.gif");
                    g.drawImage(img1, 0, 0, this.getWidth(), this.getHeight(), this);
                    img = ImageIO.read(new File(thispath + "/src/pictures/infopic.png"));
                    g.drawImage(img, 140, 50, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        };

        panelintru.setLayout(null);
        JButton backfromintro = new JButton();
        backfromintro.setText("back");
        panelintru.add(backfromintro);
        backfromintro.setBounds(810, 800, 100, 30);

        thr.start();

        keyboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Main.this.remove(infopanel);
                Main.this.setContentPane(panelkey);
                Main.this.validate();

                backfromkey.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Main.this.remove(panelkey);
                        Main.this.setContentPane(infopanel);
                        Main.this.validate();
                    }
                });
            }
        });

        backfrominfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Main.this.remove(infopanel);
                Main.this.setContentPane(panel);
                Main.this.validate();

            }
        });

        intru.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Main.this.remove(infopanel);
                Main.this.setContentPane(panelintru);
                Main.this.validate();

                backfromintro.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        Main.this.remove(panelintru);
                        Main.this.setContentPane(infopanel);
                        Main.this.validate();

                    }
                });
            }
        });

        playgame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.this.remove(panel);
                Main.this.setContentPane(playgamepanel);
                Main.this.validate();
            }
        });

        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Main.this.remove(panel);
                Main.this.setContentPane(infopanel);
                Main.this.validate();

            }
        });

        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitGame();
            }
        });

        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                tmp[0] = true;
                Main.this.dispose();
                int he = Integer.parseInt(k[0]);
                int wi = Integer.parseInt(k1[0]);
                GamePlay gp = new GamePlay(he, wi);
                gp.setVisible(true);

            }
        });

        loadGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                LoadFrame loadFrame = new LoadFrame();
                String path = loadFrame.getPath();

                if (!path.equals("")) {
                    FileInputStream fis = null;
                    ObjectInputStream in = null;
                    try {
                        Main.this.dispose();
                        GamePlay gp = new GamePlay();
                        fis = new FileInputStream(path);
                        in = new ObjectInputStream(fis);
                        gp.boardPanel.replace2this((BoardPanel) in.readObject(),true);
                        in.close();
                        gp.setVisible(true);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            }
        });

        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.this.remove(playgame);
                Main.this.setContentPane(newgamepanel);
                Main.this.validate();
            }
        });

        backInplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.this.remove(playgamepanel);
                Main.this.setContentPane(panel);
                Main.this.validate();
            }
        });

        backInNewgame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Main.this.remove(newgamepanel);
                Main.this.setContentPane(playgamepanel);
                Main.this.validate();
            }
        });

        this.setContentPane(panel);

    }

    public void playmusic() {

        ArrayList<FileInputStream> fis = new ArrayList<>();
        try {
            fis.add(new FileInputStream(thispath + "/src/music/01 Nocturne (1).mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/01 - 2+2=5 (The Lukewarm.).mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/1-02 Pyramid Song.mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/2-10 The Beginning And The End.mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/11. The Conflagration.mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/Thurisaz – Endless....mp3"));
            fis.add(new FileInputStream(thispath + "/src/music/01. Main Title Theme – Westworld.mp3"));
            int i = 2;
            while (i < fis.size()) {
                Player player = new Player(fis.get(i++));
                player.play();
                if (i == fis.size())
                    i = 0 ;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JavaLayerException e) {
            e.printStackTrace();

        }

    }

    public void exitGame() {
        System.exit(0);
    }

    public static void main(String[] args) {
        Main newMain = new Main(true);
        newMain.setVisible(true);
    }
}
